package cc.ngon.ikana;

import cc.ngon.util.Config;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

/**
 * @author: Ben Cochrane
 * Date: 7/17/13
 */
public class DesktopStarter {

    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = Ikana.NAME + " - v" + Ikana.VERSION;

        Config.useConfig("desktop", "./main/data/config/desktop.xml");

        cfg.useGL20 = Config.getBoolean("requireGL20");
        cfg.width = Config.getInt("windowWidth");
        cfg.height = Config.getInt("windowHeight");
        new LwjglApplication(new Ikana(), cfg);
    }

}
