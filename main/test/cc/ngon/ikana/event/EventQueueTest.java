package cc.ngon.ikana.event;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Ben Cochrane
 * @since 9/3/13
 */
public class EventQueueTest {

	Event e1, e2;
	EventQueue queue;

	@Before
	public void setUp() throws Exception {
		e1 = new Event(new EventDelegate() {
			@Override
			public void act() {

			}
		}, null, 1);

		e2 = new Event(new EventDelegate() {
			@Override
			public void act() {

			}
		}, null, 2);
		queue = new EventQueue();
	}

	@Test
	public void testRegister() throws Exception {
		Assert.assertEquals(0, queue.size());
		queue.register(e2);
		Assert.assertEquals(1, queue.size());
		Assert.assertEquals(e2, queue.eventQueue.first());
		queue.register(e1);
		Assert.assertEquals(2, queue.size());
		Assert.assertEquals(e1, queue.eventQueue.first());
	}

	@Test
	public void testProcessEvent() throws Exception {
		queue.register(e1);
		queue.register(e2);
		e1.setRecurring(false);
		e2.setRecurring(true);
		Assert.assertEquals(e1, queue.eventQueue.first());
		Assert.assertEquals(2, queue.size());
		queue.processEvent();
		Assert.assertEquals(e2, queue.eventQueue.first());
		Assert.assertEquals(1, queue.size());
		queue.processEvent();
		Assert.assertEquals(e2, queue.eventQueue.first());
		Assert.assertEquals(1, queue.size());


	}
}
