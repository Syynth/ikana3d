package cc.ngon.ikana.ai.path;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.AssetProvider.AssetParameters;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import junit.framework.Assert;
import org.junit.Test;

/**
 * @author Ben Cochrane
 * @since 8/14/13
 */
public class NodesetTest {

	static {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.useGL20 = true;
		new LwjglApplication(new Game() {
			@Override
			public void create() {
			}
		}, cfg);
	}

	AssetProvider assetProvider; {
		assetProvider = new AssetProvider(new AssetParameters(AssetParameters.MAPS));
	}

	@Test
	public void testSetNeighbors() throws Exception {
		Nodeset ns = new Nodeset(assetProvider.getMap("debug-map"));
		Array<Node> neighbors = ns.nodes[1][1].neighbors;
		Assert.assertEquals(6, neighbors.size);
		Assert.assertTrue(neighbors.contains(ns.nodes[0][1], true));    // Left
		Assert.assertTrue(neighbors.contains(ns.nodes[2][1], true));    // Right
		Assert.assertTrue(neighbors.contains(ns.nodes[1][0], true));    // Top
		Assert.assertTrue(neighbors.contains(ns.nodes[1][2], true));    // Bottom
		Assert.assertTrue(neighbors.contains(ns.nodes[2][2], true));    // Diagonal Right
		Assert.assertTrue(neighbors.contains(ns.nodes[0][2], true));    // Diagonal Left
	}

	@Test
	public void testCalculatePathsAt() throws Exception {
		Nodeset ns = new Nodeset(assetProvider.getMap("debug-map"));
		ns.calculatePathsAt(0, 0, new Nodeset.NodesetParameters());
		Assert.assertEquals(1f, ns.at(1, 0).getValue());
		Assert.assertEquals(2f, ns.at(2, 0).getValue());
		Assert.assertEquals(3f, ns.at(3, 0).getValue());
	}

	@Test
	public void testGetPathBetween() throws Exception {
		Nodeset ns = new Nodeset(assetProvider.getMap("debug-map"));
		ns.calculatePathsAt(0, 0);
		Path path = ns.getPathTo(3, 2);
		Assert.assertNotNull(path);
		Assert.assertEquals(0f, path.first().position.x);
		Assert.assertEquals(0f, path.first().position.y);
		Assert.assertEquals(3f, path.last().position.x);
		Assert.assertEquals(2f, path.last().position.y);
		Assert.assertEquals(5, path.length());
	}

	@Override
	public void finalize() {
		assetProvider.dispose();
	}
}
