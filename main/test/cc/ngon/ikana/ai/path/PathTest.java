package cc.ngon.ikana.ai.path;

import com.badlogic.gdx.math.Vector3;
import junit.framework.Assert;
import org.junit.Test;

/**
 * @author Ben Cochrane
 * @since 8/31/13
 */
public class PathTest {

	@Test
	public void testPushNode() throws Exception {
		Path path = new Path();
		Assert.assertEquals(0, path.length());
		Node n1 = new Node(new Vector3()), n2 = new Node(new Vector3());
		path.pushNode(n1);
		Assert.assertEquals(1, path.length());
		Assert.assertEquals(n1, path.first());
		path.pushNode(n2);
		Assert.assertEquals(2, path.length());
		Assert.assertEquals(n2, path.first());
	}

	@Test
	public void testPrevious() throws Exception {
		Path path = new Path();
		Node n1 = new Node(new Vector3()), n2 = new Node(new Vector3());
		path.pushNode(n2).pushNode(n1);
		Assert.assertEquals(n1, path.previous(n2));
	}

	@Test
	public void testNext() throws Exception {
		Path path = new Path();
		Node n1 = new Node(new Vector3()), n2 = new Node(new Vector3());
		path.pushNode(n2).pushNode(n1);
		Assert.assertEquals(n2, path.next(n1));
	}
}
