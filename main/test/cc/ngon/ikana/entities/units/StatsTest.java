package cc.ngon.ikana.entities.units;

import junit.framework.Assert;
import org.junit.Test;

/**
 * @author Ben
 * @since 9/1/13
 */
public class StatsTest {

	@Test
	public void testSum() throws Exception {
		Stats s1 = new Stats.StatsBuilder().zero().build();
		Stats s2 = new Stats.StatsBuilder().zero().attack(1f).build();
		Stats s3 = s1.sum(s2);
		Assert.assertEquals(1f, s3.attack());
	}
}
