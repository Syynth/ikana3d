package cc.ngon.ikana.entities.units;

/**
 * @author: Ben Cochrane
 * Date: 8/6/13
 */
public interface StatModifier {

	public Stats getStats();

}
