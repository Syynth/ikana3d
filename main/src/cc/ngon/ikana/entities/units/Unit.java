package cc.ngon.ikana.entities.units;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.Ikana;
import cc.ngon.ikana.entities.Entity;
import cc.ngon.ikana.entities.abilities.Ability;
import cc.ngon.ikana.entities.abilities.Walk;
import cc.ngon.ikana.event.Event;
import cc.ngon.ikana.gfx.RenderBag;
import cc.ngon.ikana.gfx.Renderable;
import cc.ngon.util.Utils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.decals.Decal;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * @author Ben Cochrane
 * @since 7/18/13
 */
public class Unit extends Entity {

	// TODO: List available options for the MapController to use
	// TODO: Categorize abilities, and restrict usage per category - (maybe parametrically?)

	protected Array<Ability> abilities;
	protected Array<Stats> levelUps;
	protected Vector3 position;
	protected Race race;
	protected Event turn;
	protected Decal sprite;
	protected ModelInstance model;
	protected float hp;

	public Unit(Vector3 position) {
		this.race = Race.Hylian;
		this.position = Utils.toHex(position.add(0, 0, 1.5f));
		this.levelUps = new Array<>();
		this.abilities = new Array<>();
	}

	public float getHp() {
		return hp;
	}

	public Unit setHp(float hp) {
		this.hp = hp;
		return this;
	}

	public Unit changeHp(float delta) {
		this.hp += delta;
		Gdx.app.log(Ikana.LOG, "Unit health changed.");
		return this;
	}

	public float calculateDamage(Unit target) {
		return Math.max(getStatsTotal().attack() - target.getStatsTotal().defense(), 0);
	}

	public boolean update(float delta) {
		boolean finished = true;
		for (Ability ability : abilities) {
			if (!ability.update(delta)) {
				finished = false;
			}
		}
		return finished;
	}

	public Unit registerAbility(Ability ability) {
		abilities.add(ability);
		return this;
	}

	public Unit unregisterAbility(Ability ability) {
		if (abilities.contains(ability, true)) {
			abilities.removeValue(ability, true);
			return this;
		}
		throw new IllegalArgumentException("Tried to unregister an Ability which this Unit did not have");
	}

	public <T extends Ability> T getAbility(Class<T> abilityClass) {
		for (Ability a : abilities) {
			if (a.getClass().isAssignableFrom(abilityClass)) {
				return (T) a;
			}
		}
		return null;
	}

	public Unit setTurn(Event turn) {
		this.turn = turn;
		return this;
	}

	public Event getTurn() {
		return turn;
	}

	public Unit setModel(AssetProvider assetProvider) {
		sprite = Decal.newDecal(1, 1, new TextureRegion(assetProvider.getTexture("unit")));
		sprite.setPosition(position.x, position.y, position.z);
		model = assetProvider.getModel("unit");
		model.transform.translate(Utils.toHex(this.position));
		model.transform.rotate(new Vector3(1, 0, 0), 90f);
		return this;
	}

	public Vector3 getPosition() {
		return this.position;
	}

	public Unit setPosition(Vector3 position) {
		this.position.set(position.x, position.y, position.z + 1.5f);
		this.sprite.setPosition(position.x, position.y, position.z + sprite.getHeight() * 1.5f);
		this.model.transform.setTranslation(this.position);
		return this;
	}

	public Stats getStatsTotal() {
		Stats total = new Stats.StatsBuilder().zero().delay(10).build();
		total = total.sum(Stats.BaseStats);
		total = total.sum(race.getStats());
		for (Stats s : levelUps) {
			total = total.sum(s);
		}
		return total;
	}

	@Override
	public Renderable render(RenderBag bag) {
		if (bag.decalBatch != null && sprite != null) {
			// TODO: Check for an ortho camera, and perform a different lookAt() to keep them parallel to camera vector
			sprite.lookAt(bag.camera.position, Vector3.Z);
			bag.decalBatch.add(sprite);
		}
		// TODO: Create an animation object
		return this;
	}

	@Override
	public Unit setZ(float z) {
		this.setPosition(new Vector3(position.x, position.y, z));
		return this;
	}
}
