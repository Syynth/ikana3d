package cc.ngon.ikana.entities.units;

/**
 * @author: Ben Cochrane
 * @since 8/6/13
 */
public class Stats {

	public static Stats BaseStats = new StatsBuilder().build();

	protected enum Stat {
		Attack,
		Defense,
		Magic,
		Resistance,
		Dexterity,
		Agility,
		Luck,

		Movement,
		Jump,
		Weight,
		Delay
	}

	protected float[] stats;

	public Stats(float[] stats) {
		this.stats = stats;
	}

	public Stats(Stats other) {
		this.stats = new float[other.stats.length];
		System.arraycopy(other.stats, 0, this.stats, 0, stats.length);
	}

	public Stats sum(Stats other) {
		Stats result = new Stats(other);
		for (int i = 0; i < stats.length; ++i) {
			result.stats[i] += stats[i];
		}
		return result;
	}

	public float attack() {
		return stats[Stat.Attack.ordinal()];
	}

	public Stats setAttack(float attack) {
		stats[Stat.Attack.ordinal()] = attack;
		return this;
	}

	public float defense() {
		return stats[Stat.Defense.ordinal()];
	}

	public Stats setDefense(float defense) {
		stats[Stat.Defense.ordinal()] = defense;
		return this;
	}

	public float magic() {
		return stats[Stat.Magic.ordinal()];
	}

	public Stats setMagic(float magic) {
		stats[Stat.Magic.ordinal()] = magic;
		return this;
	}

	public float resistance() {
		return stats[Stat.Magic.ordinal()];
	}

	public Stats setResistance(float resistance) {
		stats[Stat.Resistance.ordinal()] = resistance;
		return this;
	}

	public float dexterity() {
		return stats[Stat.Dexterity.ordinal()];
	}

	public Stats setDexterity(float dexterity) {
		stats[Stat.Dexterity.ordinal()] = dexterity;
		return this;
	}

	public float agility() {
		return stats[Stat.Agility.ordinal()];
	}

	public Stats setAgility(float agility) {
		stats[Stat.Agility.ordinal()] = agility;
		return this;
	}

	public float luck() {
		return stats[Stat.Luck.ordinal()];
	}

	public Stats setLuck(float luck) {
		stats[Stat.Luck.ordinal()] = luck;
		return this;
	}

	public float movement() {
		return stats[Stat.Movement.ordinal()];
	}

	public Stats setMovement(float movement) {
		stats[Stat.Movement.ordinal()] = movement;
		return this;
	}

	public float jump() {
		return stats[Stat.Jump.ordinal()];
	}

	public Stats setJump(float jump) {
		stats[Stat.Jump.ordinal()] = jump;
		return this;
	}

	public float weight() {
		return stats[Stat.Weight.ordinal()];
	}

	public Stats setWeight(float weight) {
		stats[Stat.Weight.ordinal()] = weight;
		return this;
	}

	public float delay() {
		return stats[Stat.Delay.ordinal()];
	}

	public Stats setDelay(float delay) {
		stats[Stat.Delay.ordinal()] = delay;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder("[");
		b.append("attack => ").append(attack()).append(", ");
		b.append("defense => ").append(defense()).append(", ");
		b.append("magic => ").append(magic()).append(", ");
		b.append("resistance => ").append(resistance()).append(", ");
		b.append("dexterity => ").append(dexterity()).append(", ");
		b.append("agility => ").append(agility()).append(", ");
		b.append("luck => ").append(luck()).append(", ");
		b.append("movement => ").append(movement()).append(", ");
		b.append("jump => ").append(jump()).append(", ");
		b.append("weight => ").append(weight()).append(", ");
		b.append("delay => ").append(delay()).append("]");
		return b.toString();
	}

	public static class StatsBuilder {

		protected float[] stats = {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};

		public StatsBuilder() {
			movement(4);
		}

		public StatsBuilder zero() {
			attack(0)
				.defense(0)
				.magic(0)
				.resistance(0)
				.dexterity(0)
				.agility(0)
				.luck(0)
				.movement(0)
				.jump(0)
				.weight(0)
				.delay(0);
			return this;
		}

		public StatsBuilder attack(float attack) {
			stats[Stat.Attack.ordinal()] = attack;
			return this;
		}

		public StatsBuilder defense(float defense) {
			stats[Stat.Defense.ordinal()] = defense;
			return this;
		}

		public StatsBuilder magic(float magic) {
			stats[Stat.Magic.ordinal()] = magic;
			return this;
		}

		public StatsBuilder resistance(float resistance) {
			stats[Stat.Resistance.ordinal()] = resistance;
			return this;
		}

		public StatsBuilder dexterity(float dexterity) {
			stats[Stat.Dexterity.ordinal()] = dexterity;
			return this;
		}

		public StatsBuilder agility(float agility) {
			stats[Stat.Agility.ordinal()] = agility;
			return this;
		}

		public StatsBuilder luck(float luck) {
			stats[Stat.Luck.ordinal()] = luck;
			return this;
		}

		public StatsBuilder movement(float movement) {
			stats[Stat.Movement.ordinal()] = movement;
			return this;
		}

		public StatsBuilder jump(float jump) {
			stats[Stat.Jump.ordinal()] = jump;
			return this;
		}

		public StatsBuilder weight(float weight) {
			stats[Stat.Weight.ordinal()] = weight;
			return this;
		}

		public StatsBuilder delay(float delay) {
			stats[Stat.Delay.ordinal()] = delay;
			return this;
		}

		public Stats build() {
			return new Stats(stats);
		}


	}

}
