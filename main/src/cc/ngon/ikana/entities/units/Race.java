package cc.ngon.ikana.entities.units;

import cc.ngon.util.Config;

/**
 * @author: Ben Cochrane
 * Date: 8/6/13
 */
public class Race implements StatModifier {

	protected Stats stats;
	protected String name;

	protected Race(String name) {
		this.name = name;
	}

	protected Race setStats() {
		stats = new Stats.StatsBuilder().zero()
			.attack(Config.getFloat(name, "attack"))
			.defense(Config.getFloat(name, "defense"))
			.magic(Config.getFloat(name, "magic"))
			.resistance(Config.getFloat(name, "resistance"))
			.dexterity(Config.getFloat(name, "dexterity"))
			.agility(Config.getFloat(name, "agility"))
			.luck(Config.getFloat(name, "luck"))
			.movement(Config.getFloat(name, "movement"))
			.jump(Config.getFloat(name, "jump"))
			.weight(Config.getFloat(name, "weight"))
			.delay(Config.getFloat(name, "delay"))
			.build();
		return this;
	}

	public boolean equals(Race race) {
		return race.name.equals(this.name);
	}

	@Override
	public Stats getStats() {
		return stats;
	}

	public static Race Hylian;
	public static Race Goron;

	static {
		Config.useConfig("races", "./main/data/config/races.xml");
		Hylian = new Race("hylian").setStats();
		Goron = new Race("goron").setStats();
	}

}
