package cc.ngon.ikana.entities.abilities;

import cc.ngon.ikana.Ikana;
import cc.ngon.ikana.ai.path.Node;
import cc.ngon.ikana.ai.path.Nodeset;
import cc.ngon.ikana.ai.path.Path;
import cc.ngon.ikana.entities.units.Unit;
import cc.ngon.ikana.map.MapModel;
import cc.ngon.util.Config;
import cc.ngon.util.Utils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * @author Ben Cochrane
 * @since 9/5/13
 */
public class Walk extends Ability<Walk.WalkParameters> {

	protected Nodeset nodeset;
	protected Path path;

	private boolean used;
	private float distance;
	private float speed;
	private WalkParameters parameters;

	public Walk(Unit unit, MapModel mapModel) {
		super(unit, mapModel);
		this.active = false;
		this.activeAbility = true;
		this.used = false;
		this.distance = 0;
		this.nodeset = new Nodeset(mapModel);
		Config.useConfig("prefs", "./main/data/config/config.xml");
		this.speed = Config.getFloat("units", "speed");
	}

	public Array<Vector3> getValidMoves() {
		Array<Vector3> moves = new Array<>();
		for (Node n : nodeset.all()) {
			if (!n.isNull() && n.getValue() <= unit.getStatsTotal().movement()) {
				moves.add(new Vector3(n.getCell().getPosition()));
			}
		}
		return moves;
	}

	@Override
	public Walk initialize(WalkParameters parameters) {
		this.parameters = parameters;
		nodeset.calculatePathsAt(parameters.gridStart);
		return this;
	}

	@Override
	public Walk activate(WalkParameters parameters) {
		this.parameters = parameters;
		int x = (int) parameters.gridEnd.x, y = (int) parameters.gridEnd.y;
		float fx, fy;
		fx = Utils.toRect(unit.getPosition()).x;
		fy = Utils.toRect(unit.getPosition()).y;
		if (mapModel.moveEntity(unit, x, y)) {
			mapModel.moveEntity(unit, (int) fx, (int) fy);
			path = nodeset.getPathTo(parameters.gridEnd);
			this.active = true;
		}
		return null;
	}

	@Override
	public boolean update(float delta) {
		if (!active && !used) {
			return false;
		}
		// TODO: One day incorporate jumping up/down into the animation.
		int x = (int) parameters.gridEnd.x, y = (int) parameters.gridEnd.y;
		if (!nodeset.at(x, y).isNull() && nodeset.at(x, y).getValue() <= unit.getStatsTotal().movement()) {
			if (distance < path.length() - 1) {
				distance += speed * delta;
				distance = Math.min(distance, path.length() - 1);
				int iF = (int) Math.floor(distance), iC = (int) Math.ceil(distance);
				float rem = distance - iF;
				Vector3 p1 = Utils.toHex(path.get(iF).getPosition()), p2 = Utils.toHex(path.get(iC).getPosition());
				mapModel.moveEntity(unit, (int) p1.x, (int) p1.y);
				p1 = new Vector3(p1);
				unit.setPosition(p1.lerp(p2, rem));
				if (distance == path.length() - 1) {
					return true;
				}
			} else {
				if (mapModel.moveEntity(unit, x, y)) {
					unit.setPosition(Utils.toHex(x, y, mapModel.getZ(x, y)));
					return true;
				}
			}
		}
		return false;
	}

	public static class WalkParameters implements AbilityParameters {

		public Vector2 gridStart;
		public Vector2 gridEnd;

		public WalkParameters() {
		}

		public WalkParameters(Vector2 gridStart) {
			this.gridStart = gridStart;
		}

		public WalkParameters setGridStart(Vector2 gridStart) {
			this.gridStart = gridStart;
			return this;
		}

		public WalkParameters setGridEnd(Vector2 gridEnd) {
			this.gridEnd = gridEnd;
			return this;
		}

	}
}
