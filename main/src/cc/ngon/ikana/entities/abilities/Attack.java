package cc.ngon.ikana.entities.abilities;

import cc.ngon.ikana.ai.path.Nodeset;
import cc.ngon.ikana.entities.units.Unit;
import cc.ngon.ikana.map.MapModel;
import cc.ngon.util.Utils;

/**
 * @author Ben Cochrane
 * @since 9/8/13
 */
public class Attack extends Ability<Attack.AttackParamters> {

	public Attack(Unit unit, MapModel mapModel) {
		super(unit, mapModel);
	}

	@Override
	public Attack initialize(AttackParamters paramters) {
		nodeset = new Nodeset(mapModel);
		return this;
	}

	@Override
	public Attack activate(AttackParamters parameters) {
		if (nodeset.at(Utils.toRect(parameters.target.getPosition())).getNeighbors().contains(
			nodeset.at(Utils.toRect(unit.getPosition())), true
		)) {
			this.active = true;
			parameters.target.changeHp(-unit.calculateDamage(parameters.target));
		}
		return this;
	}

	@Override
	public boolean update(float delta) {
		if (active)
			return true;
		return false;
	}

	public static class AttackParamters implements AbilityParameters {

		public Unit target;

		public AttackParamters() {
		}

		public AttackParamters(Unit target) {
			this.target = target;
		}

		public AttackParamters setTarget(Unit target) {
			this.target = target;
			return this;
		}

	}

}
