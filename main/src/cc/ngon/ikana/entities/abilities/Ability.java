package cc.ngon.ikana.entities.abilities;

import cc.ngon.ikana.ai.path.Nodeset;
import cc.ngon.ikana.entities.units.Unit;
import cc.ngon.ikana.map.MapModel;

/**
 * @author Ben Cochrane
 * @since 9/5/13
 */
public abstract class Ability<T extends AbilityParameters> {

	protected boolean active;
	protected boolean activeAbility;

	protected Unit unit;
	protected MapModel mapModel;
	protected Nodeset nodeset;

	public Ability(Unit unit, MapModel mapModel) {
		this.unit = unit;
		this.mapModel = mapModel;
	}

	public Ability initialize(T parameters) {
		return this;
	}

	public abstract Ability activate(T parameters);

	public abstract boolean update(float delta);

	public boolean isActive() {
		return active;
	}

	public boolean isActiveAbility() {
		return activeAbility;
	}

	public boolean isPassiveAbility() {
		return !activeAbility;
	}

}