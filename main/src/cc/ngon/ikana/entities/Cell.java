package cc.ngon.ikana.entities;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.gfx.RenderBag;
import cc.ngon.ikana.gfx.Renderable;
import cc.ngon.util.Utils;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.materials.TextureAttribute;
import com.badlogic.gdx.math.Vector3;

/**
 * Base class for all terrain cells
 *
 * @author: Ben Cochrane
 * Date: 7/17/13
 */
public class Cell implements Renderable {

	protected ModelInstance model;
	protected Vector3 position;
	protected float startZ;
	protected Type type;

	public Cell(Vector3 position, String type) {
		this.position = position;
		this.startZ = position.z;
		this.type = Type.Grass;
		for (Type t : Type.values()) {
			if (t.name().equalsIgnoreCase(type)) {
				this.type = t;
			}
		}
	}

	public Cell setModel(AssetProvider assetProvider) {
		String name = "grass";
		if (type == Type.Null) {
			return this;
		}
		model = assetProvider.getModel("cell");
		switch (type) {
			case Grass:
				name = "grass";
				break;
			case Rock:
				name = "rock";
				break;
		}
		model.materials.get(1).set(TextureAttribute.createDiffuse(assetProvider.getTexture(name)));
		model.transform.translate(Utils.toHex(position));
		model.transform.rotate(new Vector3(1, 0, 0), 90f);
		return this;
	}

	public Type getType() {
		return type;
	}

	public ModelInstance getModel() {
		return model;
	}

	public Vector3 getPosition() {
		return position;
	}

	@Override
	public Renderable render(RenderBag bag) {
		if (model != null) {
			bag.batch.render(this.getModel(), bag.lights);
		}
		return this;
	}

	public Cell setZ(float z) {
		this.position.set(position.x, position.y, z);
		this.model.transform.setTranslation(Utils.toHex(position));
		return this;
	}

	public float getStartZ() {
		return startZ;
	}

	public enum Type {
		Grass,
		Rock,
		Null,
	}

}
