package cc.ngon.ikana.entities;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.gfx.Renderable;

/**
 * @author: Ben Cochrane
 * Date: 7/19/13
 */
public abstract class Entity implements Renderable {

	public abstract Entity setZ(float z);

	public abstract Entity setModel(AssetProvider assetProvider);

}
