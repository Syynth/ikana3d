package cc.ngon.ikana;

import cc.ngon.ikana.map.MapFactory;
import cc.ngon.util.Config;
import com.badlogic.gdx.Game;

/**
 * @author: Ben Cochrane
 * @date: 7/17/13
 */
public class Ikana extends Game {

	@Override
	public void create() {
		Config.useConfig("prefs", "./main/data/config/config.xml");
		this.setScreen(new MapFactory().createMap("debug-map"));
	}

	public static final String NAME = "Ikana";
	public static final String VERSION = "0.0.4";
	public static final String LOG = "Ikana";
}
