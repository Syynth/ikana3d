package cc.ngon.ikana.map;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.entities.Entity;
import cc.ngon.ikana.entities.units.Unit;
import cc.ngon.ikana.event.Event;
import cc.ngon.ikana.event.EventDelegate;
import cc.ngon.ikana.event.EventQueue;
import cc.ngon.ikana.map.control.CameraController;
import cc.ngon.ikana.map.control.MapController;
import cc.ngon.ikana.map.control.UnitController;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

/**
 * Map is the central point for all data and interaction during gameplay.
 *
 * @author Ben Cochrane
 * @since 7/24/13
 */
public class Map implements Screen {

	/**
	 * A pointer to the {@link MapModel} data containing the state of the level.
	 */
	protected MapModel mapModel;
	/**
	 * A pointer to the level's {@link MapView}, which contains the logic for rendering, as well as the camera state.
	 */
	protected MapView mapView;
	/**
	 * A pointer to a subclass of {@link MapController}, responsible for manipulating events on the map.
	 */
	protected MapController mapController;
	/**
	 * A pointer to the {@link CameraController} responsible for manipulating the MapView's Camera object.
	 */
	protected CameraController cameraController;
	/**
	 * A pointer to the {@link AssetProvider} to be used for loading all assets present in the current Map.
	 */
	protected AssetProvider assetProvider;
	/**
	 * A pointer to the {@link EventQueue} instance representing the current state of upcoming events.
	 */
	protected EventQueue queue;

	/**
	 * Default/Only constructor for the Map class, requiring pre-configured Model and View instances.
	 *
	 * @param mapModel         MapModel instance to be used for the lifetime of the Map object.
	 * @param mapView          MapView instance to be used for the lifetime of the Map object.
	 * @param cameraController CameraController instance to be updated by {@link #mapController}
	 * @param assetProvider    AssetProvider instance to be used for the lifetime of the Map object.
	 */
	public Map(MapModel mapModel, MapView mapView, CameraController cameraController, AssetProvider assetProvider) {
		this.mapModel = mapModel;
		this.mapView = mapView;
		this.cameraController = cameraController;
		this.assetProvider = assetProvider;
		this.mapController = null;
		this.queue = new EventQueue();
		this.mapView.registerRenderable(this.cameraController.cursor());
	}

	/**
	 * Call this to ready the event queue, so the main game loop can progress.
	 *
	 * @return Returns <code>this</code> for method chaining.
	 */
	public Map ready() {
		Entity[][] entities = mapModel.getEntities();
		for (Entity[] es : entities) {
			for (final Entity e : es) {
				if (e != null && e.getClass() == Unit.class) {
					queue.register(((Unit) e).setTurn(newTurn((Unit) e)).getTurn());
				}
			}
		}
		queue.processEvent();
		return this;
	}

	/**
	 * Call this method from {@link Map#mapController} when finished.
	 * <p/>
	 * Calling this method will cause the event queue to process the next event, and likely pop the current {@link MapController}.
	 *
	 * @return Returns <code>this</code> for method chaining.
	 */
	public Map controllerFinished() {
		queue.processEvent();
		return this;
	}

	/**
	 * Generates a turn {@link Event} for a {@link Unit}.
	 *
	 * @param unit The Unit that the turn will be generated for.
	 * @return The new turn Event.
	 */
	public Event newTurn(final Unit unit) {
		final Map map = this;
		final CameraController cameraController = this.cameraController;
		final AssetProvider assetProvider = this.assetProvider;
		return new Event(new EventDelegate() {
			@Override
			public void act() {
				registerMapController(new UnitController(map, cameraController, assetProvider, unit));
			}
		}, queue, (int) unit.getStatsTotal().delay()).setRecurring(true);
	}

	/**
	 * Overwrites the {@link #mapController} with the supplied {@link MapController}.
	 * <p>
	 * This method also provides cleanup for the old controller and setup for the new controller, by calling their
	 * {@link cc.ngon.ikana.map.control.MapController#pop()} and {@link cc.ngon.ikana.map.control.MapController#push()}
	 * methods respectively. Lastly, the MapController is set to be the new InputProcess for {@link Gdx#input}.
	 * </p>
	 *
	 * @param mapController The new MapController to be used for the Map.
	 * @return Returns <code>this</code> for method chaining.
	 */
	public Map registerMapController(MapController mapController) {
		if (this.mapController != null) {
			this.mapController.pop();
		}
		this.mapController = mapController;
		this.mapController.push();
		Gdx.input.setInputProcessor(this.mapController);
		return this;
	}

	/**
	 * @return Returns a reference this {@link #mapModel}.
	 */
	public MapModel model() {
		return mapModel;
	}

	/**
	 * @return Returns a reference to {@link #mapView}.
	 */
	public MapView view() {
		return mapView;
	}


	@Override
	public void render(float delta) {
		mapController.update(delta);
		mapView.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		mapView.resize(width, height);
	}

	@Override
	public void dispose() {
		mapView.dispose();
	}

	@Override
	public void show() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
}
