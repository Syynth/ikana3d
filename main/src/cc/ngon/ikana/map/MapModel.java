package cc.ngon.ikana.map;

import cc.ngon.ikana.entities.Cell;
import cc.ngon.ikana.entities.Entity;
import cc.ngon.ikana.entities.units.Unit;
import com.badlogic.gdx.math.Vector2;

/**
 * @author Ben Cochrane
 * @since 7/17/13
 */
public class MapModel {

	// TODO: Overwrite #clone, so the AssetProvider can keep an 'ideal' copy of the MapModel
	// TODO: Create a member Nodeset object, so it can be efficiently reused.

	protected Vector2 size;
	protected Cell[][] terrain;
	protected Entity[][] entities;

	public MapModel(Vector2 size) {
		this.size = size;
		this.terrain = new Cell[(int) Math.floor(size.x)][(int) Math.floor(size.y)];
		this.entities = new Unit[terrain.length][terrain[0].length];
	}

	public Vector2 size() {
		return size;
	}

	public Cell[][] getTerrain() {
		return terrain;
	}

	public Entity[][] getEntities() {
		return entities;
	}

	public MapModel setTerrain(Cell terrain, int x, int y) {
		this.terrain[x][y] = terrain;
		return this;
	}

	public MapModel setEntity(Entity entity, int x, int y) {
		this.entities[x][y] = entity;
		return this;
	}

	public boolean moveEntity(Entity entity, int toX, int toY) {
		if (inBounds(toX, toY)) {
			for (int i = 0; i < entities.length; ++i) {
				for (int j = 0; j < entities[i].length; ++j) {
					Entity e = entities[i][j];
					if (e == null) {
						continue;
					}
					if (e.equals(entity) && entities[toX][toY] == null) {
						entities[toX][toY] = e;
						entities[i][j] = null;
						return true;
					}
				}
			}
		}
		return false;
	}

	public float getZ(float x, float y) {
		if (inBounds(x, y)) {
			if (terrain[(int) Math.floor(x)][(int) Math.floor(y)] != null)
				return terrain[(int) Math.floor(x)][(int) Math.floor(y)].getPosition().z;
		}
		return 0.f;
	}

	public MapModel setZ(float x, float y, float z) {
		if (inBounds(x, y)) {
			Cell c = terrain[(int) Math.floor(x)][(int) Math.floor(y)];
			Entity e = entities[(int) Math.floor(x)][(int) Math.floor(y)];
			if (c != null) {
				c.setZ(z);
				if (e != null)
					e.setZ(z);
			}
		}
		return this;
	}

	public MapModel resetZ(float x, float y) {
		if (inBounds(x, y)) {
			Cell c = terrain[(int) Math.floor(x)][(int) Math.floor(y)];
			if (c != null) {
				c.setZ(c.getStartZ());
			}
		}
		return this;
	}

	public float getZ(Vector2 pos) {
		return getZ(pos.x, pos.y);
	}

	boolean inBounds(float x, float y) {
		return Math.floor(x) >= 0 && Math.floor(x) < size.x && Math.floor(y) >= 0 && Math.floor(y) < size.y;
	}

}
