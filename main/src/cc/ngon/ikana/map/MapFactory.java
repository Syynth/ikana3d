package cc.ngon.ikana.map;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.entities.Cell;
import cc.ngon.ikana.entities.Entity;
import cc.ngon.ikana.map.control.CameraController;

/**
 * @author Ben Cochrane
 * @since 8/16/13
 */
public class MapFactory {

	AssetProvider assetProvider;
	MapView mapView;
	MapModel mapModel;
	CameraController cameraController;

	public MapFactory() {
	}

	public Map createMap(String name) {
		assetProvider = new AssetProvider();
		mapView = new MapView();
		mapModel = assetProvider.getMap(name);
		setModels();
		cameraController = new CameraController(mapModel, mapView);
		return new Map(mapModel, mapView, cameraController, assetProvider).ready();
	}

	private void setModels() {
		Cell[][] terrain = mapModel.getTerrain();
		Entity[][] entities = mapModel.getEntities();
		for (Cell[] ts : terrain) {
			for (Cell t : ts) {
				if (t != null) {
					mapView.registerRenderable(t.setModel(assetProvider));
				}
			}
		}
		for (Entity[] es : entities) {
			for (Entity e : es) {
				if (e != null) {
					mapView.registerRenderable(e.setModel(assetProvider));
				}
			}
		}
	}

}
