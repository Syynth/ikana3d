package cc.ngon.ikana.map.control;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.ai.path.Node;
import cc.ngon.ikana.ai.path.Nodeset;
import cc.ngon.ikana.entities.abilities.Attack;
import cc.ngon.ikana.entities.abilities.Walk;
import cc.ngon.ikana.entities.units.Unit;
import cc.ngon.ikana.map.Map;
import cc.ngon.ikana.ui.TileOverlay;
import cc.ngon.util.Config;
import cc.ngon.util.Utils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * @author Ben Cochrane
 * @since 7/26/13
 */
public class UnitController extends MapController {

	protected Unit unit;
	protected Array<TileOverlay> overlays;
	// TODO: Add a Stack<InputProcessors> to contain the Cursor, and Unit Ability Dialog

	public UnitController(Map map, CameraController cameraController, AssetProvider assetProvider, Unit unit) {
		super(map, cameraController, assetProvider);
		this.unit = unit;
		overlays = new Array<>();
	}

	@Override
	public MapController update(float delta) {
		cameraController.update(delta);
		if (unit.update(delta)) {
			map.controllerFinished();
		}
		return null;
	}

	@Override
	public MapController pop() {
		for (TileOverlay t : overlays) {
			map.view().unregisterRenderable(t);
		}
		unit.unregisterAbility(unit.getAbility(Walk.class));
		return this;
	}

	@Override
	public MapController push() {
		cameraController.cursor().setPosition(unit.getPosition());
		cameraController.cursor().pos().z = map.model().getZ(cameraController.cursor().gridPos());

		unit.registerAbility(new Walk(unit, map.model()));
		unit.registerAbility(new Attack(unit, map.model()));
		unit.getAbility(Walk.class).initialize(new Walk.WalkParameters(cameraController.cursor.gridPos()));
		unit.getAbility(Attack.class).initialize(new Attack.AttackParamters());

		for (Vector3 v : unit.getAbility(Walk.class).getValidMoves()) {
			TileOverlay t = new TileOverlay(assetProvider, Utils.toHex(v.add(0, 0, 1.01f)));
			overlays.add(t);
			map.view().registerRenderable(t);
		}
		return this;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO: This will have to be redone eventually; keyDown instead of keyTyped
		switch (character) {
			case 'q':
				cameraController.rotate(60 * Utils.degtorad);
				break;
			case 'e':
				cameraController.rotate(-60 * Utils.degtorad);
				break;
			case 'w':
				cameraController.addDistance(-2.5f);
				break;
			case 's':
				cameraController.addDistance(2.5f);
				break;
			case '\t':
				map.controllerFinished();
				break;
			case ' ':
				unit.getAbility(Walk.class).activate(
					new Walk.WalkParameters().setGridEnd(cameraController.cursor.gridPos()));
				break;

			// TODO: If this is even going to remain an option, it needs to be purely a setting, not dynamic like this.
			case 'o':
				map.view().setPerspective(false);
				cameraController.setDistance(Config.getFloat("camera", "isoViewDistance"));
				break;
			case 'p':
				map.view().setPerspective(true);
				cameraController.setDistance(Config.getFloat("camera", "perspectiveViewDistance"));
				break;
		}
		return true;
	}

}
