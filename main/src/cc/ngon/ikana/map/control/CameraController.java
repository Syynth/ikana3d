package cc.ngon.ikana.map.control;

import cc.ngon.ikana.map.MapModel;
import cc.ngon.ikana.map.MapView;
import cc.ngon.ikana.ui.Cursor;
import cc.ngon.util.Config;
import cc.ngon.util.Utils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * @author: Ben Cochrane
 * Date: 8/16/13
 */
public class CameraController {

	protected Vector3 look;
	protected float distance;
	protected Vector2 angle;
	protected float lerp;
	protected Cursor cursor;
	protected MapView view;
	protected MapModel model;

	public CameraController(MapModel mapModel, MapView mapView) {
		this.model = mapModel;
		this.view = mapView;
		cursor = new Cursor(new Vector3());
		Config.useConfig("prefs", "./main/data/config/config.xml");
		this.lerp = Config.getFloat("camera", "transitionSpeed");
		boolean isP = Config.getBoolean("camera", "perspective");
		this.distance = isP ? Config.getFloat("camera", "perspectiveViewDistance") :
			Config.getFloat("camera", "isoViewDistance");
		this.angle = new Vector2((isP ? Config.getFloat("camera", "perspectiveX") : Config.getFloat("camera", "isoX")) * Utils.degtorad,
			(isP ? Config.getFloat("camera", "perspectiveY") : Config.getFloat("camera", "isoY")) * Utils.degtorad);
		this.look = new Vector3(cursor.pos());
	}

	public CameraController update(float delta) {
		cursor.update(angle, delta);
		cursor.pos().z = model.getZ(cursor.gridPos()) + 0.5f;
		view.camera().position.lerp(new Vector3(
			cursor.pos().x + (float) Math.cos(angle.x) * distance,
			cursor.pos().y + (float) Math.sin(angle.x) * distance,
			cursor.pos().z + (float) Math.sin(angle.y) * distance), lerp * delta);
		look.lerp(cursor.pos(), lerp * delta);
		view.camera().lookAt(look);
		view.camera().up.slerp(new Vector3(0, 0, 1), 0.9f);
		view.camera().update();
		return this;
	}

	public CameraController rotate(float angle) {
		this.angle.x += angle;
		return this;
	}

	public void addDistance(float distance) {
		this.distance += distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public Cursor cursor() {
		return cursor;
	}

}
