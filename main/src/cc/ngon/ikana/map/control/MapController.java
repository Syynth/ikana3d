package cc.ngon.ikana.map.control;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.map.Map;
import com.badlogic.gdx.InputProcessor;

/**
 * @author Ben Cochrane
 * @since 7/26/13
 */
public abstract class MapController implements InputProcessor {

	protected Map map;
	protected CameraController cameraController;
	protected AssetProvider assetProvider;

	public MapController(Map map, CameraController cameraController, AssetProvider assetProvider) {
		this.map = map;
		this.cameraController = cameraController;
		this.assetProvider = assetProvider;
	}

	public abstract MapController pop();

	public abstract MapController push();

	public abstract MapController update(float delta);


	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
