package cc.ngon.ikana.map;

import cc.ngon.ikana.gfx.RenderBag;
import cc.ngon.ikana.gfx.Renderable;
import cc.ngon.util.Config;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.decals.CameraGroupStrategy;
import com.badlogic.gdx.graphics.g3d.decals.DecalBatch;
import com.badlogic.gdx.graphics.g3d.lights.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.lights.Lights;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * @author: Ben Cochrane
 * @Date: 7/24/13
 */
public class MapView {

	protected ModelBatch batch;
	protected DecalBatch decalBatch;
	protected Lights lights;
	protected Camera camera;
	protected Array<Renderable> renderables;

	public MapView() {
		this.renderables = new Array<>();
		this.batch = new ModelBatch();
		this.decalBatch = new DecalBatch();
		this.lights = new Lights();
		this.lights.ambientLight.set(1f, 1f, 1f, 1f);
		this.lights.add(new DirectionalLight().set(0, 0, 0, -10f, -1f, 0.5f));
		Config.useConfig("prefs", "./main/data/config/config.xml");
		setPerspective(Config.getBoolean("camera", "perspective"));
		Gdx.gl.glClearColor(0.f, 0.f, 0.f, 1.f);
		Gdx.gl.glEnable(GL10.GL_ALPHA_TEST);
	}

	public Camera camera() {
		return camera;
	}

	public MapView registerRenderable(Renderable renderable) {
		if (!renderables.contains(renderable, false)) {
			renderables.add(renderable);
		}
		return this;
	}

	public MapView unregisterRenderable(Renderable renderable) {
		if (renderables.contains(renderable, false)) {
			renderables.removeValue(renderable, false);
		}
		return this;
	}

	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		Gdx.gl10.glAlphaFunc(GL10.GL_GREATER, 0);

		batch.begin(camera);
		RenderBag bag = new RenderBag(delta).addBatch(batch).addLights(lights).addDecalBatch(decalBatch).addCamera(camera);
		for (Renderable r : renderables) {
			r.render(bag);
		}
		batch.end();
		decalBatch.flush();
	}

	public MapView resize(int width, int height) {
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.viewportWidth = width;
		camera.viewportHeight = height;
		camera.update();
		render(0f);
		return this;
	}

	public MapView dispose() {
		batch.dispose();
		decalBatch.dispose();
		return this;
	}

	public boolean isPerspective() {
		return camera.getClass().equals(PerspectiveCamera.class);
	}

	public void setPerspective(boolean perspective) {
		Config.useConfig("prefs", "./main/data/config/config.xml");
		Vector3 pos = new Vector3(0, 0, 5), look = new Vector3(0, 0, 0);
		if (this.camera != null) {
			pos = camera.position;
			look = camera.direction;
		}
		if (perspective) {
			this.camera = new PerspectiveCamera(67f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		} else {
			this.camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
			((OrthographicCamera) camera).zoom = Config.getFloat("camera", "isoZoom");
		}
		this.camera.position.set(pos);
		this.camera.up.set(0, 0, 1);
		this.camera.direction.set(look);
		this.camera.update();
		this.camera.near = Config.getFloat("camera", "znear");
		this.camera.far = Config.getFloat("camera", "zfar");
		this.decalBatch.setGroupStrategy(new CameraGroupStrategy(camera));
	}

}
