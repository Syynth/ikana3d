package cc.ngon.ikana.map;

import cc.ngon.ikana.entities.Cell;
import cc.ngon.ikana.entities.units.Unit;
import cc.ngon.util.Utils;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.File;

/**
 * @author: Ben Cochrane
 * @date: 7/20/13
 */
public class MapLoader extends SynchronousAssetLoader<MapModel, MapLoader.MapModelParameter> {

	public MapLoader(FileHandleResolver resolver) {
		super(resolver);
	}

	MapModel mm = null;

	@Override
	public MapModel load(AssetManager manager, String fileName, FileHandle handle, MapModelParameter parameter) {
		try {
			File f = resolve(fileName).file();
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(f);
			Element element = doc.getDocumentElement();
			mm = new MapModel(new Vector2(Utils.i(element.getAttribute("width")), Utils.i(element.getAttribute("height"))));
			XPathFactory xpf = XPathFactory.newInstance();
			XPath xPath = xpf.newXPath();
			NodeList nl = (NodeList) xPath.evaluate("//data/terrain/tile", doc, XPathConstants.NODESET);

			for (int i = 0; i < nl.getLength(); ++i) {
				Node n = nl.item(i);
				Vector3 vector3 = new Vector3(Utils.i(((Element) n).getAttribute("x")), Utils.i(((Element) n).getAttribute("y")), Utils.f(((Element) n).getAttribute("z")));
				mm.setTerrain(new Cell(vector3, ((Element) n).getAttribute("type")), (int) vector3.x, (int) vector3.y);
			}

			nl = (NodeList) xPath.evaluate("//data/entities/unit", doc, XPathConstants.NODESET);

			for (int i = 0; i < nl.getLength(); ++i) {
				Node n = nl.item(i);
				Vector3 vector3 = new Vector3(Utils.i(((Element) n).getAttribute("x")), Utils.i(((Element) n).getAttribute("y")), 0);
				vector3.z = mm.getZ(vector3.x, vector3.y);
				mm.setEntity(new Unit(vector3), (int) vector3.x, (int) vector3.y);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mm;
	}

	@Override
	public Array<AssetDescriptor> getDependencies(String fileName, FileHandle handle, MapModelParameter parameter) {
		return null;
	}

	public static class MapModelParameter extends AssetLoaderParameters<MapModel> {

	}
}
