package cc.ngon.ikana;

import cc.ngon.ikana.map.MapLoader;
import cc.ngon.ikana.map.MapModel;
import cc.ngon.util.Config;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;

import java.util.HashMap;
import java.util.Map;

/**
 * AssetProvider wraps and handles all calls to and from the libGDX assetManager.
 *
 * @author Ben Cochrane
 * @since 8/16/2013
 */
public class AssetProvider {

	// TODO: Create an animation XML format

	/**
	 * Reference to the libGDX AssetManager object.
	 */
	protected AssetManager assetManager;
	/**
	 * Contains a mapping of 'code-friendly' names to their respective MapModel assets.
	 * i.e., 'level-1' -> './main/data/maps/level-1.final.xml'
	 */
	protected HashMap<String, String> maps;
	/**
	 * Contains a mapping of 'code-friendly' names to their respective Model assets.
	 * i.e., 'playerStand' -> './main/data/models/player/stand-pose.obj'
	 */
	protected HashMap<String, String> models;
	/**
	 * Contains a mapping of 'code-friendly' names to their respective Texture assets.
	 * i.e, 'playerHurt' -> './main/data/textures/enemy/player-hurt.png'
	 */
	protected HashMap<String, String> textures;
	/**
	 * A copy of the {@link cc.ngon.ikana.AssetProvider.AssetParameters} object passed in upon creation.
	 * <p/>
	 * parameters are cached to prevent access to {@link #getMap(String)}, {@link #getModel(String)},
	 * or {@link #getTexture(String)} as required.
	 */
	protected AssetParameters parameters;

	/**
	 * Default Constructor. Acts as a proxy for {@link #AssetProvider(AssetParameters)}.
	 * <p/>
	 * Default constructor will construct {@link AssetParameters} with {@link AssetParameters#ALL}.
	 */
	public AssetProvider() {
		this(new AssetParameters(AssetParameters.ALL));
	}

	/**
	 * Loads the Assets indicated by the passed in {@link AssetParameters}
	 *
	 * @param parameters Stored in {@link this.parameters}, and used to determine which types of assets to load.
	 */
	public AssetProvider(AssetParameters parameters) {

		this.parameters = parameters;

		assetManager = new AssetManager();
		assetManager.setLoader(MapModel.class, new MapLoader(new InternalFileHandleResolver()));

		// TODO: Parametrize where this file is found.
		Config.useConfig("assets", "./main/data/config/assets.xml");

		if (parameters.maps()) {
			maps = loadAssetGroup("maps", MapModel.class);
		}
		if (parameters.models()) {
			models = loadAssetGroup("models", Model.class);
		}
		if (parameters.textures()) {
			textures = loadAssetGroup("textures", Texture.class);
		}

		assetManager.finishLoading();
	}

	/**
	 * Simply a helper method to load all the assets from a given directory
	 * <p/>
	 * This method creates a HashMap, mapping 'code-friendly' names to asset paths,
	 * and returns it to be stored in the member variables of the class.
	 *
	 * @param name      The name of the type of assets, i.e., "maps", or "models".
	 * @param classType The class of the assets after they are loaded, i.e., "MapModel.class"
	 * @param <T>       Generics indicator - required by the compiler to avoid implicit conversion.
	 * @return Returns the HashMap containing 'code-friendly' names mapped to their asset paths.
	 */
	private <T> HashMap<String, String> loadAssetGroup(String name, Class<T> classType) {
		String path = Config.getProperty("directories", name);
		HashMap<String, String> map = Config.getPropertyGroup(name);
		for (Map.Entry<String, String> entry : map.entrySet()) {
			assetManager.load(path + entry.getValue(), classType);
			entry.setValue(path + entry.getValue());
		}
		return map;
	}

	/**
	 * Creates a {@link com.badlogic.gdx.graphics.g3d.ModelInstance} made from the model represented by the <code>name</code> parameter.
	 *
	 * @param name The name of the Model from which to create the desired ModelInstance. This should be the 'code-friendly' name.
	 * @return A ModelInstance object constructed from the Model specified.
	 */
	public ModelInstance getModel(String name) {
		if (parameters.models())
			return new ModelInstance(assetManager.get(models.get(name), Model.class));
		else
			throw new IllegalStateException("Cannot Access Models - AssetProvider was constructed without them!");
	}

	/**
	 * Finds a {@link com.badlogic.gdx.graphics.Texture} corresponding to the <code>name</code> parameter.
	 *
	 * @param name The name of the Texture desired. This should be the 'code-friendly' name.
	 * @return A reference to the Texture instance.
	 */
	public Texture getTexture(String name) {
		if (parameters.textures())
			return assetManager.get(textures.get(name), Texture.class);
		else
			throw new IllegalStateException("Cannot Access Textures - AssetProvider was constructed without them!");
	}

	/**
	 * Finds a {@link cc.ngon.ikana.map.MapModel} instance corresponding to the <code>name</code> parameter.
	 *
	 * @param name The name of the MapModel desired. This should be the 'code-friendly' name.
	 * @return A reference to the MapModel instance.
	 */
	public MapModel getMap(String name) {
		if (parameters.maps())
			return assetManager.get(maps.get(name), MapModel.class);
		else
			throw new IllegalStateException("Cannot Access MapModels - AssetProvider was constructed without them!");
	}

	/**
	 * A proxy call for {@link com.badlogic.gdx.assets.AssetManager#dispose()}.
	 */
	public void dispose() {
		assetManager.dispose();
	}

	/**
	 * A Class representing the configuration options for the AssetManager class.
	 *
	 * @author Ben Cochrane
	 * @since 8/29/2013
	 */
	public static class AssetParameters {

		/**
		 * A flag representing the intent to load {@link MapModel} instances via the {@link AssetProvider}
		 */
		public static final int MAPS = 0b000000000000001;
		/**
		 * A flag representing the intent to load Texture instances via the {@link AssetProvider}
		 */
		public static final int TEXTURES = 0b000000000000010;
		/**
		 * A flag representing the intent to load Model instances via the {@link AssetProvider}
		 */
		public static final int MODELS = 0b000000000000100;
		/**
		 * A convenience flag representing the union of all asset types. This is set by default.
		 */
		public static final int ALL = AssetParameters.MAPS | AssetParameters.TEXTURES | AssetParameters.MODELS;
		/**
		 * Data to store the flags during the lifetime of the {@link AssetProvider}.
		 */
		int params;

		/**
		 * Default Constructor
		 * <p/>
		 * Calls {@link #AssetParameters(int)} with a value of {@link AssetParameters#ALL}.
		 */
		public AssetParameters() {
			this(AssetParameters.ALL);
		}

		/**
		 * Real Constructor
		 *
		 * @param params The flags to be stored in {@link AssetParameters#params}
		 */
		public AssetParameters(int params) {
			this.params = params;
		}

		/**
		 * Checks to see if the {@link AssetParameters#MAPS} bit is on.
		 *
		 * @return Returns boolean, indicating whether or not to load maps.
		 */
		public boolean maps() {
			return (params & MAPS) == MAPS;
		}

		/**
		 * Checks to see if the {@link AssetParameters#TEXTURES} bit is on.
		 *
		 * @return Returns boolean, indicating whether or not to load textures.
		 */
		public boolean textures() {
			return (params & TEXTURES) == TEXTURES;
		}

		/**
		 * Checks to see if the {@link AssetParameters#MODELS} bit is on.
		 *
		 * @return Returns boolean, indicating whether or not to load models.
		 */
		public boolean models() {
			return (params & MODELS) == MODELS;
		}

	}

}
