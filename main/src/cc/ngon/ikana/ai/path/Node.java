package cc.ngon.ikana.ai.path;

import cc.ngon.ikana.entities.Cell;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * Node is a pure data model representing a {@link Cell} in the {@link cc.ngon.ikana.map.MapModel}.
 * <p>
 * The Node class essentially acts as a container for the metadata that needs to be associate with each {@link Cell} in
 * order to do the path finding work that {@link Nodeset} does. It is also useful to any class that must work with the
 * Nodeset for path finding.
 * </p>
 *
 * @author: Ben Cochrane
 * @since 8/12/13
 */
public class Node implements Comparable<Node> {

	/**
	 * The {@link Cell} that this Node represents.
	 */
	Cell cell;
	/**
	 * The effective distance from the start point on the board.
	 */
	float value;
	/**
	 * The Nodes directly accessible via this Node.
	 */
	Array<Node> neighbors;
	/**
	 * This acts as a proxy for {@link Cell#position}, in the event the {@link #cell} value is <code>null</code>.
	 */
	Vector3 position;

	/**
	 * The constructor to be used if {@link #isNull()} should return false.
	 * <p>
	 * This will construct a Node which contains a cell.
	 * </p>
	 *
	 * @param cell
	 */
	public Node(Cell cell) {
		this.cell = cell;
		this.position = cell.getPosition();
		this.value = -1f;
		this.neighbors = new Array<>();
	}

	/**
	 * The constructor to be used if {@link #isNull()} should return true.
	 * <p>
	 * This will construct a Node which represents an empty cell in the Map.
	 * </p>
	 *
	 * @param position The position in the Map which <i>would</i> have contained a Cell, were there any.
	 */
	public Node(Vector3 position) {
		this.position = position;
		this.cell = null;
		this.value = -1f;
		this.neighbors = new Array<>();
	}

	/**
	 * @return This Node's position vector.
	 */
	public Vector3 getPosition() {
		return position;
	}

	/**
	 * @return This Node's {@link Cell} reference.
	 */
	public Cell getCell() {
		return cell;
	}

	/**
	 * The {@link #neighbors} array is not guaranteed to be in any order.
	 *
	 * @return This Node's list of neighbors.
	 */
	public Array<Node> getNeighbors() {
		return neighbors;
	}

	/**
	 * @return This Node's effective distance from the Map's current 'start' point.
	 */
	public float getValue() {
		return value;
	}

	/**
	 * @return Whether this Node has a valid {@link Cell} reference.
	 */
	public boolean isNull() {
		return cell == null;
	}

	@Override
	public int compareTo(Node node) {
		return (int) (100f * value) - (int) (100f * node.value);
	}
}