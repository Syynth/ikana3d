package cc.ngon.ikana.ai.path;

import com.badlogic.gdx.utils.Array;

/**
 * The Path class acts as data structure representing a traversal beginning at
 * {@link Nodeset#calculatePathsAt(int, int)}, and ending at {@link Nodeset#getPathTo(com.badlogic.gdx.math.Vector2)}.
 * <p/>
 * The Path class is essentially a thin layer of abstraction for an {@link Array} containing the {@link Node} instances.
 * These instances are ordered, and do not have any knowledge of each other, aside from containing one another in their
 * {@link Node#neighbors} array.
 * </P><P>
 * An important point to note is that the {@link #first()} <i>must</i> contain the {@link Node} located in {@link Nodeset}
 * at {@link Nodeset#calculatePathsAt(int, int)}, as well as the {@link Node} located at {@link Nodeset#getPathTo(float, float)}
 * </P>
 *
 * @author Ben Cochrane
 * @since 8/13/13
 */
public class Path {

	/**
	 * The interal {@link Array} that the Path class is built upon.
	 */
	protected Array<Node> nodes;

	/**
	 * Default Constructor. Initializes {@link #nodes}.
	 */
	public Path() {
		nodes = new Array<>();
	}

	/**
	 * Simple constructor to initialize {@link #nodes} to contain the <code>node</code> passed in.
	 *
	 * @param node The node with which to
	 */
	public Path(Node node) {
		nodes = new Array<>();
		nodes.add(node);
	}

	/**
	 * This method is for {@link Nodeset} to add nodes to the beginning of the {@link Array}.
	 * <p>
	 * The reason this must be is that when the {@link Nodeset} walks the Nodes, it must start with the specified endpoint,
	 * and walk down the graph to the start point monotonically.
	 * </p>
	 *
	 * @param node The {@link Node} to be pushed on the start of the Path.
	 * @return Returns <code>this</code> for method chaining.
	 */
	Path pushNode(Node node) {
		nodes.insert(0, node);
		return this;
	}

	/**
	 * @param i
	 * @return
	 */
	public Node get(int i) {
		return nodes.get(i);
	}

	/**
	 * Returns the first {@link Node} in the Path.
	 *
	 * @return The Node located at the beginning of the Path.
	 */
	public Node first() {
		if (nodes.size != 0) {
			return nodes.get(0);
		}
		return null;
	}

	/**
	 * Returns the last {@link Node} in the Path.
	 *
	 * @return The Node located at the beginning of the Path.
	 */
	public Node last() {
		if (nodes.size > 0) {
			return nodes.get(nodes.size - 1);
		}
		return null;
	}

	/**
	 * Finds the previous Node to the instance passed in.
	 * <p>
	 * If the {@link Node} does not exist in the Path, or is the first Node in the Path, this method returns <code>null</code>.
	 * </p>
	 *
	 * @param node Node instance in the Path.
	 * @return The previous Node, if any exists, and <code>null</code> otherwise.
	 */
	public Node previous(Node node) {
		if (nodes.contains(node, true)) {
			int index = nodes.indexOf(node, true);
			if (index > 0) {
				return nodes.get(index - 1);
			}
		}
		return null;
	}

	/**
	 * Finds the next Node following the instance passed in.
	 * <p>
	 * If the {@link Node} does not exist in the Path, or is the last Node in the Path, this method returns <code>null</code>.
	 * </p>
	 *
	 * @param node Node instance in the Path.
	 * @return The next Node, if any exists, and <code>null</code> otherwise.
	 */
	public Node next(Node node) {
		if (nodes.contains(node, true)) {
			int index = nodes.indexOf(node, true);
			if (index < nodes.size) {
				return nodes.get(index + 1);
			}
		}
		return null;
	}

	/**
	 * Returns the number of Nodes in the Path.
	 *
	 * @return Number of Nodes in the path.
	 */
	public int length() {
		return nodes.size;
	}

}
