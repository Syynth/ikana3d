package cc.ngon.ikana.ai.path;

import cc.ngon.ikana.entities.Cell;
import cc.ngon.ikana.map.MapModel;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

/**
 * Nodeset is a simple implementation of the 'blob-map' algorithm.
 *
 * @author Ben Cochrane
 * @since 8/12/13
 */
public class Nodeset {

	protected Node[][] nodes;
	protected MapModel mapModel;

	public Nodeset(MapModel mapModel) {
		this.mapModel = mapModel;
		Cell[][] terrain = mapModel.getTerrain();
		this.nodes = new Node[terrain.length][terrain[0].length];
		for (int i = 0; i < nodes.length; ++i) {
			for (int j = 0; j < nodes[i].length; ++j) {
				if (terrain[i][j] != null) {
					nodes[i][j] = new Node(terrain[i][j]);
				} else {
					nodes[i][j] = new Node(new Vector3(i, j, 0));
				}
			}
		}
		for (Node[] ns : nodes) {
			for (Node n : ns) {
				setNeighbors(n);
			}
		}
	}

	public Array<Node> all() {
		Array<Node> nodes = new Array<>();
		for (Node[] ns : this.nodes) {
			for (Node n : ns) {
				nodes.add(n);
			}
		}
		return nodes;
	}

	protected void setNeighbors(Node n) {
		if (n.cell == null) {
			return;
		}
		Vector3 pos = n.cell.getPosition();
		Node top = at((int) pos.x, (int) pos.y - 1);
		Node bottom = at((int) pos.x, (int) pos.y + 1);
		Node left = at((int) pos.x - 1, (int) pos.y);
		Node right = at((int) pos.x + 1, (int) pos.y);
		int dir = ((int) pos.x & 1) == 0 ? -1 : 1;
		Node a = at((int) pos.x + 1, (int) pos.y + dir);
		Node b = at((int) pos.x - 1, (int) pos.y + dir);
		if (top != null) {
			n.neighbors.add(top);
		}
		if (bottom != null) {
			n.neighbors.add(bottom);
		}
		if (left != null) {
			n.neighbors.add(left);
		}
		if (right != null) {
			n.neighbors.add(right);
		}
		if (a != null) {
			n.neighbors.add(a);
		}
		if (b != null) {
			n.neighbors.add(b);
		}
	}

	public Node at(Vector3 xy) {
		return at((int) xy.x, (int) xy.y);
	}

	public Node at(int x, int y) {
		if (x >= 0 && y >= 0) {
			if (x < nodes.length && y < nodes[0].length) {
				if (nodes[x][y].cell != null)
					return nodes[x][y];
			}
		}
		return null;
	}

	public Nodeset calculatePathsAt(int x, int y, NodesetParameters parameters) {
		Node root = nodes[x][y];
		root.value = 0;
		defineValueAround(root);
		return this;
	}

	public Nodeset calculatePathsAt(int x, int y) {
		return calculatePathsAt(x, y, new NodesetParameters());
	}

	public Nodeset calculatePathsAt(Vector2 location) {
		return calculatePathsAt((int) location.x, (int) location.y);
	}

	public Nodeset calculatePathsAt(Vector2 location, NodesetParameters parameters) {
		return calculatePathsAt((int) location.x, (int) location.y, parameters);
	}

	private Node defineValueAround(Node node) {
		// TODO: Account for changes in grid z
		// TODO: Account for changes in grid cell type
		// TODO: Account for the ability to jump an additional grid cell to circumvent obstacles
		for (Node n : node.neighbors) {
			if (n.value == -1 || n.value > node.value + 1) {
				n.value = node.value + 1;

				defineValueAround(n);
			}
		}
		return null;
	}

	public Path getPathTo(Vector2 location) {
		return getPathTo(location.x, location.y);
	}

	public Path getPathTo(float x, float y) {
		Path path = new Path();
		path.pushNode(nodes[(int) x][(int) y]);
		getLowestNode(path, nodes[(int) x][(int) y]);
		return path;
	}

	// TODO: Handle disconnected grids.
	protected void getLowestNode(Path path, Node node) {
		node.neighbors.sort();
		if (node.value != 0) {
			path.pushNode(node.neighbors.first());
			getLowestNode(path, node.neighbors.first());
		}
	}

	public static class NodesetParameters {

		boolean height;
		boolean cellType;

		public NodesetParameters() {
			this.cellType = false;
			this.height = false;
		}

		public NodesetParameters useHeight(boolean height) {
			this.height = height;
			return this;
		}

		public NodesetParameters useCellType(boolean cellType) {
			this.cellType = cellType;
			return this;
		}

	}

}
