package cc.ngon.ikana.event;

import com.badlogic.gdx.utils.Array;

/**
 * @author Ben Cochrane
 * @since 7/19/13
 */
public class EventQueue {

	Array<Event> eventQueue;

	public EventQueue() {
		eventQueue = new Array<>();
	}

	public int size() {
		return eventQueue.size;
	}

	public EventQueue register(Event event) {

		if (eventQueue.size == 0) {
			eventQueue.add(event);
			event.setQueue(this);
		} else {
			for (int i = 0; i < eventQueue.size; ++i) {
				if (event.getPriority() < eventQueue.get(i).getPriority()) {
					eventQueue.insert(i, event);
					event.setQueue(this);
					return this;
				}
			}
			event.setQueue(this);
			eventQueue.add(event);
		}
		return this;
	}

	public EventQueue unregister(Event event) {
		if (eventQueue.removeValue(event, false)) {
			event.setQueue(null);
		}
		return this;
	}

	public EventQueue processEvent() {
		Event e = eventQueue.first();
		e.getDelegate().act();
		eventQueue.removeValue(e, true);
		for (Event ev : eventQueue) {
			ev.addPriority(-e.getPriority());
		}
		if (e.isRecurring()) {
			e.setPriority(e.getStartPriority());
		}
		return this;
	}

}
