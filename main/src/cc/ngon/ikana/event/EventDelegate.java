package cc.ngon.ikana.event;

/**
 * @author Ben Cochrane
 * @since 7/19/13
 */
public interface EventDelegate {

	public void act();

}
