package cc.ngon.ikana.event;

import cc.ngon.ikana.Ikana;
import com.badlogic.gdx.Gdx;

import java.util.Comparator;

/**
 * The Event class encapsulates a {@link EventDelegate} and adds metadata.
 * <p>
 * <p/>
 * </p>
 *
 * @author Ben Cochrane
 * @since 7/19/13
 */
public class Event implements Comparable<Event> {

	private int priority;
	private int startPriority;
	private boolean recurring;
	private EventDelegate delegate;
	private EventQueue queue;

	public Event(EventDelegate delegate, EventQueue queue, int priority) {
		this.delegate = delegate;
		this.priority = priority;
		this.queue = queue;
		this.startPriority = priority;
	}

	Event setQueue(EventQueue queue) {
		this.queue = queue;
		return this;
	}

	public boolean isRecurring() {
		return recurring;
	}

	public Event setRecurring(boolean recurring) {
		this.recurring = recurring;
		return this;
	}

	public int getStartPriority() {
		return startPriority;
	}

	public Event setStartPriority(int startPriority) {
		this.startPriority = startPriority;
		return this;
	}

	public int getPriority() {
		return priority;
	}

	/**
	 * WARNING - This method will update it's position in the {@link #queue} it belongs to.
	 * <p>
	 * If <code>this.queue</code> is <code>null</code>, the event's priority is simply overwritten.
	 * </p>
	 *
	 * @param priority The new priority to set the event to.
	 * @return Returns <code>this</code> for method chaining.
	 */
	public Event setPriority(int priority) {
		this.priority = priority;
		if (queue != null) {
			queue.unregister(this).register(this);
		}
		return this;
	}

	public Event addPriority(int amount) {
		return setPriority(priority + amount);
	}

	public EventDelegate getDelegate() {
		return delegate;
	}


	@Override
	public int compareTo(Event o) {
		return this.priority - o.priority;
	}
}
