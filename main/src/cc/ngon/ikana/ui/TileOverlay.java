package cc.ngon.ikana.ui;

import cc.ngon.ikana.AssetProvider;
import cc.ngon.ikana.gfx.RenderBag;
import cc.ngon.ikana.gfx.Renderable;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.decals.Decal;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Ben Cochrane
 * @since 8/18/13
 */
public class TileOverlay extends Widget {

	Decal decal;

	public TileOverlay(AssetProvider assetProvider, Vector3 position) {
		this.decal = Decal.newDecal(2.1f, 2.1f, new TextureRegion(assetProvider.getTexture("tile-overlay")));
		this.decal.setRotation(Vector3.Z, Vector3.Z);
		this.decal.setBlending(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		this.decal.setPosition(position.x, position.y, position.z);
	}

	@Override
	public Renderable render(RenderBag bag) {
		bag.decalBatch.add(decal);
		return this;
	}
}
