package cc.ngon.ikana.ui;

import cc.ngon.ikana.gfx.RenderBag;
import cc.ngon.ikana.gfx.Renderable;
import cc.ngon.util.Config;
import cc.ngon.util.Utils;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.materials.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.materials.Material;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * @author Ben Cochrane
 * @since 7/26/13
 */
public class Cursor extends Widget {

	// TODO: This should become an interface with a Player/AI controlled implementations
	// TODO: This should implement InputAdapter or whatever, and have the MapController multiplex the input handling.

	protected Model modelTemplate;
	protected ModelInstance model, model2;
	protected Vector3 position;
	protected float speed;

	public Cursor(Vector3 position) {
		this.position = position;
		// TODO: Maybe this should eventually be an actual model?
		ModelBuilder bldr = new ModelBuilder();
		modelTemplate = bldr.createCone(0.25f, 0.25f, 0.25f, 4,
			new Material("color", ColorAttribute.createDiffuse(Color.WHITE)),
			VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
		model = new ModelInstance(modelTemplate);
		model.transform.translate(position);
		model.transform.rotate(new Vector3(1, 0, 0), -90);
		// TODO: Make this some sort of highlight, maybe a white hexel ring
		model2 = new ModelInstance(bldr.createCylinder(0.02f, 0.02f, 100f, 8,
			new Material("color", ColorAttribute.createDiffuse(Color.RED)),
			VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal));
		Config.useConfig("prefs", "./main/data/config/config.xml");
		this.speed = Config.getFloat("cursor", "speed");
	}

	protected Cursor translateX(float x) {
		setPosition(new Vector3(position.x + x, position.y, position.z));
		return this;
	}

	protected Cursor translateY(float y) {
		setPosition(new Vector3(position.x, position.y + y, position.z));
		return this;
	}

	public Cursor update(Vector2 angle, float delta) {
		float s = speed * delta;
		boolean pressed = false;
		if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
			translateY((float) -Math.sin(angle.x) * s);
			translateX((float) -Math.cos(angle.x) * s);
			pressed = true;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
			translateY((float) Math.sin(angle.x) * s);
			translateX((float) Math.cos(angle.x) * s);
			pressed = true;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
			translateY((float) -Math.sin(angle.x + 90 * Utils.degtorad) * s);
			translateX((float) -Math.cos(angle.x + 90 * Utils.degtorad) * s);
			pressed = true;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
			translateY((float) Math.sin(angle.x + 90 * Utils.degtorad) * s);
			translateX((float) Math.cos(angle.x + 90 * Utils.degtorad) * s);
			pressed = true;
		}

		model2.transform.setTranslation(getGoal(position));


		if (!pressed) {
			position.lerp(getGoal(position), 0.1f);
		}
		model.transform.setTranslation(new Vector3(position.x, position.y, position.z + 0.7f));
		return this;
	}

	private Vector3 getGoal(Vector3 position) {
		Vector3 goal = new Vector3(position);

		float r = (float) (1 / Math.sqrt(3));
		float s = 1.5f * r;

		goal.x = s * (float) Math.round(position.x / s);
		if ((Math.round(goal.x / r * 2) & 1) == 0) {
			goal.y = (float) Math.round(position.y);
		} else {
			goal.y = (float) Math.round(position.y - .5f) + .5f;
		}

		return goal;
	}

	public Cursor setPosition(Vector3 position) {
		this.position = position;
		model.transform.setTranslation(new Vector3(position.x, position.y, position.z));
		return this;
	}

	public Vector3 pos() {
		return position;
	}

	public Vector2 gridPos() {
		Vector3 goal = getGoal(position);
		return new Vector2(Math.round(goal.x * 1.1547005384f), (int) Math.floor(goal.y));
	}

	@Override
	public Renderable render(RenderBag bag) {
		bag.batch.render(this.model, bag.lights);
		return this;
	}

}
