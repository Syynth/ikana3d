package cc.ngon.ikana.gfx;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.decals.DecalBatch;
import com.badlogic.gdx.graphics.g3d.lights.Lights;

/**
 * @author: Ben Cochrane
 * Date: 7/26/13
 */
public class RenderBag {

	public ModelBatch batch;
	public Lights lights;
	public Camera camera;
	public float delta;
	public DecalBatch decalBatch;

	public RenderBag(float delta) {
		this.delta = delta;
		batch = null;
		lights = null;
		camera = null;
		decalBatch = null;
	}

	public RenderBag addDecalBatch(DecalBatch decalBatch) {
		this.decalBatch = decalBatch;
		return this;
	}

	public RenderBag addBatch(ModelBatch batch) {
		this.batch = batch;
		return this;
	}

	public RenderBag addLights(Lights lights) {
		this.lights = lights;
		return this;
	}

	public RenderBag addCamera(Camera camera) {
		this.camera = camera;
		return this;
	}

}
