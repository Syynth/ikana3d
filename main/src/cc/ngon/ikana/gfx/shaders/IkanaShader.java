package cc.ngon.ikana.gfx.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.Renderable;
import com.badlogic.gdx.graphics.g3d.Shader;
import com.badlogic.gdx.graphics.g3d.materials.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.materials.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.RenderContext;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.GdxRuntimeException;

/**
 * @author: Ben Cochrane
 * Date: 8/8/13
 */
public class IkanaShader implements Shader {

	protected ShaderProgram program;
	protected String vertexPath;
	protected String fragmentPath;
	protected Camera camera;
	protected RenderContext context;

	int u_projTrans;
	int u_worldTrans;
	int u_color;
	int u_texture;

	public IkanaShader(String vertexPath, String fragmentPath) {
		this.vertexPath = vertexPath;
		this.fragmentPath = fragmentPath;
	}

	@Override
	public void init() {
		String vert = Gdx.files.internal(vertexPath).readString();
		String frag = Gdx.files.internal(fragmentPath).readString();
		program = new ShaderProgram(vert, frag);
		if (!program.isCompiled()) {
			throw new GdxRuntimeException(program.getLog());
		}
		u_projTrans = program.getUniformLocation("u_projTrans");
		u_worldTrans = program.getUniformLocation("u_worldTrans");
		u_texture = program.getUniformLocation("u_texture");
		u_color = program.getUniformLocation("u_color");
	}

	@Override
	public int compareTo(Shader other) {
		return 0;
	}

	@Override
	public boolean canRender(Renderable instance) {
		return instance.material.has(ColorAttribute.Diffuse);
	}

	@Override
	public void begin(Camera camera, RenderContext context) {
		this.camera = camera;
		this.context = context;
		program.begin();
		program.setUniformMatrix(u_projTrans, camera.combined);
		program.setUniformf(u_color, MathUtils.random(), MathUtils.random(), MathUtils.random());

		//context.setDepthTest(true, GL20.GL_LEQUAL);   // This needs to be updated to keep pace with the latest nightly
		context.setCullFace(GL20.GL_BACK);
	}

	@Override
	public void render(Renderable renderable) {
		program.setUniformMatrix(u_worldTrans, renderable.worldTransform);
		Color color = ((ColorAttribute) renderable.material.get(ColorAttribute.Diffuse)).color;
		program.setUniformf(u_color, color.r, color.g, color.b);
		context.textureBinder.bind(((TextureAttribute) renderable.material.get(TextureAttribute.Diffuse)).textureDescription);
		renderable.mesh.render(program, renderable.primitiveType, renderable.meshPartOffset, renderable.meshPartSize);
	}

	@Override
	public void end() {
		program.end();
	}

	@Override
	public void dispose() {
		program.dispose();
	}
}
