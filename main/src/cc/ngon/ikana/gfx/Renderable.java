package cc.ngon.ikana.gfx;

/**
 * @author: bcochrane
 * Date: 7/26/13
 */
public interface Renderable {

	public Renderable render(RenderBag bag);

}
