package cc.ngon.util;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cc.ngon.ikana.Ikana;

import com.badlogic.gdx.Gdx;

/**
 * @author Ben Cochrane
 */
public final class Config {

	// TODO: Long term this should be turned into something that (1) isn't static, and (2) is more in line with real usage patterns
	private static Document config;
	private static HashMap<String, Document> docs = new HashMap<>();
	private static HashMap<Document, String> docLocations = new HashMap<>();

	private Config() {
	}

	public static boolean useConfig(String name, String path) {
		if (docs.containsKey(name)) {
			Config.config = docs.get(name);
			return true;
		}
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
				.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder;
			builder = factory.newDocumentBuilder();
			Config.docs.put(name, builder.parse(new File(path)));
			Config.config = docs.get(name);
			Config.docLocations.put(Config.config, path);
			return true;
		} catch (ParserConfigurationException | SAXException | IOException ex) {
			Gdx.app.log(Ikana.LOG, ex.toString());
		}
		return false;
	}

	public static String getProperty(String name) {
		return getProperty("global", name);
	}

	public static String getProperty(String group, String name) {
		if (Config.config == null) {
			return "";
		} else {
			XPathFactory factory = XPathFactory.newInstance();
			XPath path = factory.newXPath();
			try {
				return (String) path.evaluate("//propertyGroup[@name='" + group
					+ "']/property[@name='" + name + "']/text()", Config.config, XPathConstants.STRING);
			} catch (XPathExpressionException ex) {
				Gdx.app.log(Ikana.LOG, ex.toString());
				return null;
			}
		}
	}

	public static HashMap<String, String> getPropertyGroup(String groupName) {
		HashMap<String, String> group = new HashMap<>();
		if (Config.config == null) {
			return group;
		}
		try {
			XPathFactory factory = XPathFactory.newInstance();
			XPath path = factory.newXPath();
			NodeList nl = (NodeList) path.evaluate("//propertyGroup[@name='" + groupName + "']/property", Config.config, XPathConstants.NODESET);
			for (int i = 0; i < nl.getLength(); ++i) {
				Element n = (Element) nl.item(i);
				group.put(n.getAttribute("name"), n.getTextContent());
			}
		} catch (XPathExpressionException ex) {
			Gdx.app.log(Ikana.LOG, "Error parsing propertyGroup.", ex);
		}
		return group;
	}

	public static boolean getBoolean(String name) {
		return Boolean.parseBoolean(getProperty(name));
	}

	public static boolean getBoolean(String group, String name) {
		return Boolean.parseBoolean(getProperty(group, name));
	}

	public static int getInt(String name) {
		return Integer.parseInt(getProperty(name));
	}

	public static int getInt(String group, String name) {
		return Integer.parseInt(getProperty(group, name));
	}

	public static float getFloat(String name) {
		return Float.parseFloat(getProperty(name));
	}

	public static float getFloat(String group, String name) {
		return Float.parseFloat(getProperty(group, name));
	}

	public static double getDouble(String name) {
		return Double.parseDouble(getProperty(name));
	}

	public static double getDouble(String group, String name) {
		return Double.parseDouble(getProperty(group, name));
	}

	public static boolean setProperty(String name, String value) {
		return setProperty("global", name, value);
	}

	public static boolean setProperty(String group, String name, String value) {
		XPathFactory xfac = XPathFactory.newInstance();
		XPathExpression xpr;
		XPath xpath = xfac.newXPath();
		try {
			xpr = xpath.compile("//propertyGroup[@name='" + group
				+ "']/property[@name='" + name + "']/text()");
			Node n = (Node) xpr.evaluate(Config.config, XPathConstants.NODE);
			n.setNodeValue(value);
			return XmlDocumentWriter.write(config);
		} catch (XPathExpressionException ex) {
			Gdx.app.log(Ikana.LOG, ex.toString());
			return false;
		}
	}

	private static class XmlDocumentWriter {

		public static boolean write(Document doc) {
			TransformerFactory factory = TransformerFactory.newInstance();
			try {
				Transformer transformer = factory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(docLocations.get(config)));
				transformer.transform(source, result);
			} catch (TransformerConfigurationException e) {
				e.printStackTrace();
			} catch (TransformerException e) {
				e.printStackTrace();
			}
			return false;
		}
	}
}