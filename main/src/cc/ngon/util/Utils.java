package cc.ngon.util;

import com.badlogic.gdx.math.Vector3;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author: Ben Cochrane
 * Date: 7/18/13
 */
public class Utils {

	public static final float degtorad = 0.01745329252f;
	public static final float radtodeg = 57.2957795f;

	public static String fileAsString(String path) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded)).toString();
	}

	public static String fileAsString(File file) throws IOException {
		byte[] encoded = Files.readAllBytes(file.toPath());
		return StandardCharsets.UTF_8.decode(ByteBuffer.wrap(encoded)).toString();
	}

	public static int i(String s) {
		return Integer.parseInt(s);
	}

	public static float f(String s) {
		return Float.parseFloat(s);
	}

	public static Vector3 toHex(Vector3 position) {
		Vector3 hex = new Vector3();
		hex.x = position.x * ((float) Math.sqrt(3) / 2);
		hex.y = (((int) Math.floor(position.x)) & 1) == 0 ? position.y : position.y + 0.5f;
		hex.z = position.z;
		return hex;
	}

	public static Vector3 toHex(float x, float y, float z) {
		return toHex(new Vector3(x, y, z));
	}

	public static Vector3 toRect(Vector3 position) {
		Vector3 rect = new Vector3();
		rect.x = position.x / ((float) Math.sqrt(3) / 2);
		rect.y = (((int) Math.floor(position.y)) & 1) == 0 ? position.y : position.y - 0.5f;
		rect.z = position.z;
		return rect;
	}

	public static Vector3 toRect(float x, float y, float z) {
		return toRect(new Vector3(x, y, z));
	}

}
